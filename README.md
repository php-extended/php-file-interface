# php-extended/php-file-interface

Interfaces to secure the manfileulation of files in file systems, in order to
be resistant to RFI (Remote File Inclusion).

![coverage](https://gitlab.com/php-extended/php-file-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-file-interface ^8`


## Basic Usage

This library is an interface-only library.

For a concrete implementatfilen, see [php-extended/php-file-object](https://gitlab.com/php-extended/php-file-object).


## License

MIT (See [license file](LICENSE)).
